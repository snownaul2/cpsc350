use project;
insert into location values
 (23, 'ME', 'Augusta', '04926', '34 Allwey', ''),
 (12, 'CT', 'Bridgeport', '06604', '126 Park Avenue', '37 Rennell Street'),
 (19, 'NY', 'New York', '01234', '1 Random Street', 'P.O. Box 24'),
 (14, 'CT', 'Bristol', '00023', '12 Bell Rd', 'Bellheights Apartment 17'),
 (13, 'NJ', 'Providence', '99087', '74 Lewis Ave.',''),
 (20, 'MA', 'Boston', '77860', '26 Hill Street', 'Apartment 2B'),
 (21, 'PA', 'Hershey', '33456', '89 Hershy Hill Rd.','');
 
insert into payment_method values
 (1, 1, '000-000-0000', 'Lucy', 'Williams', '2025-12-29', '257', 12),
 (2, 0, '100-100-1000', 'Spencer', 'Hunt', '2027-02-28', '316', 23),
 (3, 1, '202-002-2002', 'Alice', 'White', '2026-03-24', '112', 19),
 (4, 0, '300-000-0003', 'Whitney', 'Allistor', '2025-01-28', '223', 13),
 (5, 1, '400-444-0004', 'Jim', 'Tyler', '2024-07-29', '457', 20),
 (6, 0, '500-005-0000', 'Calob', 'Pick', '2023-08-30', '214', 21),
 (7, 1, '600-660-0060', 'Ned', 'Felp', '2024-10-22', '334', 14); 
 
 insert into user values
 (3, 'Lucy', 'Williams', '2253654', 0, 'lucy@gmail.com', md5('1234'), 12),
 (4, 'Spencer', 'Hunt', '3450002', 1, 'sh.repo@outlook.com', md5('1234'), 23),
 (5, 'Alice', 'White', '8897856', 0, "aliceW@gmail.com", md5('1234'), 19),
 (6, 'Whitney', 'Allistor', '8965583', 0, 'therealdeal@yahoo.com', md5('1234'), 13),
 (7, 'Jim', 'Tyler', '3545642', 1, 'jam.ty.@gmail.com', md5('1234'), 20),
 (8, 'Calob', 'Pick', '9963210', 0, 'jhfs@outlook.com', md5('1234'), 21),
 (9, 'Ned', 'Felp', '3252132', 2, 'nedfel@yahoo.com', md5('1234'), 14),
 (10, 'Kelly', 'Hunt', '8897856', 0, 'kh.repo@outlook.com', md5('1234'), 23),
 (11, 'Peter', 'Griff', '2565246', 2, 'petergriff1M@yahoo.com', md5('1234'), 13);
 

 
 insert into bank_account values
 (0, '0000000', 'Wells Fargo', '10101001000100'),
 (1, '0203847', 'ABD Banking', '24454665185352'),
 (2, '1561586', 'HDG Holdings LLC', '56646546548921'),
 (3, '5648646', 'BF Checking', '87979589977856'),
 (4, '4646652', 'ABD Banking', '69787669720014'),
 (8, '0264023', 'Wells Fargo', '48977797810707'),
 (5, '5494687', 'HUK Global Corp.', '12517774616070');
 
 insert into seller values
 (10, 'Kelly', 0),
 (3, 'Lulu', 1),
 (7, 'Jam', 2),
 (4, 'Spence', 3),
 (11, 'Pete', 4),
 (8, 'Cal', 8),
 (9, 'Ned', 5);
 
 insert into bidder values
 (3, 1),
 (7, 5),
 (11, 4),
 (9, 7),
 (10, 2),
 (4, 2),
 (8, 6);
 
insert into admin values
 (5, 32000, '000-00-0000', '2018-12-14'),
 (6, 45000, '111-11-1111', '2014-06-04'),
 (3, 12000, '222-22-2222', '2020-09-15'),
 (8, 7000, '333-33-3333', '2021-06-24'),
 (10, 25000, '444-44-4444', '2019-08-29'),
 (9, 15000, '555-55-5555', '2022-02-20'),
 (11, 25000, '666-66-6666', '2022-03-08');
 
 insert into category values
 (0, 'Computers'),
 (1, 'Air Conditioner'),
 (2, 'Microwave'),
 (3, 'Refrigerator'),
 (4, 'Water Purifier'),
 (5, 'Blender'),
 (6, 'Washing Machine');
 
 -- need to add the highest_bid_id after adding bid rows.
 insert into item values
 (0, 'SM Monitor', 'Black 24 in.', '2022-05-24', 150.00, 0, 10,null),
 (1, 'LG Monitor', 'Silver 43 in.', '2022-08-18', 300.00, 0, 10,null),
 (2, 'Window AC', 'multicolor', '2022-05-09', 350.00, 1, 3,null),
 (3, 'PC Tower', 'Acer 2T memory Black', '2022-07-15', 600.00, 0, 3,null),
 (4, 'Fresh Microwave', 'black', '2023-04-12', 30.00, 2, 7,null),
 (5, 'Samsung refrigerator', 'smart white', '2022-04-29', 50.00, 3, 8,null),
 (6, 'Smart TV', 'Samsung 42in', '2022-06-25', 250.00, 4, 4,null);
 
insert into bid values
 (0, 200.00, 3, 0, 2),
 (1, 10.00, 5, 2, 0),
 (2, 850.00, 7, 3, 2),
 (3, 150.00, 9, 0, 1),
 (4, 40.00, 4, 4, 0),
 (5, 50.00, 4, 5, 1),
 (6, 400.00, 11, 6, 2);
 
 UPDATE item SET id_highest_bid=(SELECT bid.id_bid FROM bid WHERE bid_amount = (SELECT max(bid_amount) FROM bid WHERE id_item='0')) where id_item='0';
 UPDATE item SET id_highest_bid=(SELECT bid.id_bid FROM bid WHERE bid_amount = (SELECT max(bid_amount) FROM bid WHERE id_item='1')) where id_item='1';
 UPDATE item SET id_highest_bid=(SELECT bid.id_bid FROM bid WHERE bid_amount = (SELECT max(bid_amount) FROM bid WHERE id_item='2')) where id_item='2';
 UPDATE item SET id_highest_bid=(SELECT bid.id_bid FROM bid WHERE bid_amount = (SELECT max(bid_amount) FROM bid WHERE id_item='3')) where id_item='3';
 UPDATE item SET id_highest_bid=(SELECT bid.id_bid FROM bid WHERE bid_amount = (SELECT max(bid_amount) FROM bid WHERE id_item='4')) where id_item='4';
 UPDATE item SET id_highest_bid=(SELECT bid.id_bid FROM bid WHERE bid_amount = (SELECT max(bid_amount) FROM bid WHERE id_item='5')) where id_item='5';
 UPDATE item SET id_highest_bid=(SELECT bid.id_bid FROM bid WHERE bid_amount = (SELECT max(bid_amount) FROM bid WHERE id_item='6')) where id_item='6';
 
 insert into picture values
 (0, 'http://sm_monitor\black_24\KH_Repo' , 0),
 (1, 'http://lg_montior\black_43\KH_Repo' , 1),
 (2, 'http://mp_mulit_03420\AW' , 2),
 (3, 'http://pc_tr\2t_mem_blk\AW' , 3),
 (4, 'http://sc_calc\blk_04900\office_supply' , 4),
 (5, 'http://hd_wireless_bluetooth\white\entertaintment_bin' , 5),
 (6, 'http://smrt_tv\smsg_42\entertainment_bin', 6);
 
 insert into bookmark values
 (3, 6),
 (11, 2),
 (5, 0),
 (8, 1),
 (4, 0),
 (10, 3),
 (7, 4);
 
 insert into payment values
 (0, 4, 0, 0, 2),
 (1, 5, 2, 0, 3),
 (2, 7, 3, 1, 5),
 (3, 9, 0, 1, 7),
 (4, 8, 4, 0, 6),
 (5, 4, 5, 1, 1),
 (6, 11, 6, 1, 4);
 
 insert into shipping values
 (2, 23, 0, '00AB'),
 (3, 19, 1, '00AC'),
 (5, 20, 1, '00AD'),
 (6, 21, 0, '00AE'),
 (7, 14, 2, '00AF'),
 (1, 23, 2, '00AG'),
 (4, 13, 2, '00AH');
 
 insert into comment values
 (1, 0, 3, 'how much RAM?'),
 (2, 0, 11, 'what is the model number?'),
 (3, 6, 9, 'is it led?'),
 (4, 3, 4, 'does it come in other colors?'),
 (5, 2, 8, 'what are the graphics?'),
 (6, 4, 6, 'what brand is it?'),
 (7, 3, 10, 'what is it made out of?');
 
 insert into chatroom values
 (1, 8, 3, 3, 0),
 (2, 4, 10, 1, 0),
 (3, 4, 3, 0, 0),
 (4, 11, 4, 6, 0),
 (5, 7, 10, 1, 1),
 (6, 9, 10, 1, 0),
 (7, 4, 10, 1, 1);


insert into message values
('how is the condition?',1,8,'2022-04-10 12:45:56',1),
('It is great.',1,3,'2022-04-11 15:47:56',1),
('can I get some more pictures?',6,9,'2022-04-10 12:45:56',1),
('sure',6,10,'2022-04-12 12:47:56',1),
('There is a problem',5,7,'2022-04-10 12:45:56',1),
('Hello',1,8,'2022-04-17 12:45:56',1),
('when did you buy this?',4,11,'2022-04-12 12:45:56',0);

insert into report values
 (1, 4, 3, 'suspicious activity', 1),
 (2, 4, 5, 'rip off', 0),
 (3, 9, 6, 'harassment', 1),
 (4, 9, 8, 'cheap knock off', 0),
 (5, 9, 5, 'suspicious behavior', 1),
 (6, 9, 3, 'overcharged my account', 1),
 (7, 9, 5, 'misconduct', 1);