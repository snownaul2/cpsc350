use project;
-- Location
CREATE TABLE location
(id_location			INTEGER			NOT NULL,
state					VARCHAR(45)		NOT NULL,
city					VARCHAR(45)		NOT NULL,
zip_code				VARCHAR(45)			NOT NULL,
street_address1			VARCHAR(45)		NOT NULL,
street_address2			VARCHAR(45),
PRIMARY KEY (id_location)
);

-- Payment_Method
CREATE TABLE payment_method
(id_pmethod		INTEGER		NOT NULL,
check_or_debit	INTEGER		NOT NULL,
card_number		VARCHAR(45)		NOT NULL,
f_name		varchar(45)		NOT NULL,
l_name		varchar(45)		NOT NULL,
exp_date		DATE			NOT NULL,
cvc			VARCHAR(45)		NOT NULL,
id_location		INTEGER		NOT NULL,
PRIMARY KEY (id_pmethod),
FOREIGN KEY (id_location) REFERENCES location(id_location) ON DELETE CASCADE
);


-- User
CREATE TABLE user
(id_user			INTEGER		NOT NULL,
f_name				VARCHAR(45)		NOT NULL,
l_name				VARCHAR(45)		NOT NULL,
phone				VARCHAR(45)		NOT NULL,
status				INTEGER,
email				VARCHAR(45)		NOT NULL,
password			varchar(45)		NOT NULL,
id_location			INTEGER		NOT NULL,
PRIMARY KEY (id_user),
FOREIGN KEY (id_location) REFERENCES location(id_location) ON DELETE CASCADE
);


-- Bank_Account
CREATE TABLE bank_account
(id_bank_account			INTEGER		NOT NULL,
routing_num				VARCHAR(45)		NOT NULL,
bank_name				VARCHAR(45)		NOT NULL,
account_num				VARCHAR(45)		NOT NULL,
PRIMARY KEY (id_bank_account)
);


-- Seller
CREATE TABLE seller
(id_user				INTEGER		NOT NULL,
seller_nickname			VARCHAR(45)		NOT NULL,
id_bank_account			INTEGER		NOT NULL,
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE,
FOREIGN KEY (id_bank_account) REFERENCES bank_account(id_bank_account) ON DELETE CASCADE
);


-- Bidder
CREATE TABLE bidder
(id_user			INTEGER		NOT NULL,
id_pmethod			INTEGER		NOT NULL,
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE,
FOREIGN KEY (id_pmethod) REFERENCES payment_method(id_pmethod) ON DELETE CASCADE
);


-- Admin
CREATE TABLE admin
(id_user			INTEGER		NOT NULL,
salary				INTEGER,
ssn				VARCHAR(45)		NOT NULL,
started_date			DATE		NOT NULL,
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE
);


-- Category
CREATE TABLE category
(id_category		INTEGER		NOT NULL,
name			VARCHAR(45)		NOT NULL,
PRIMARY KEY (id_category)
);


-- Item
CREATE TABLE item
(id_item			INTEGER		NOT NULL,
name				VARCHAR(45)		NOT NULL,
description			VARCHAR(45)		NOT NULL,
deadline			DATE		NOT NULL,
min_price			DOUBLE		NOT NULL,
id_category			INTEGER		NOT NULL,
id_user			INTEGER		NOT NULL,
PRIMARY KEY (id_item),
FOREIGN KEY (id_category) REFERENCES category(id_category) ON DELETE CASCADE,
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE
);


-- Bid
CREATE TABLE bid
(id_bid			INTEGER		NOT NULL,
bid_amount		DOUBLE		NOT NULL,
id_user			INTEGER		NOT NULL,
id_item			INTEGER		NOT NULL,
status			INTEGER		NOT NULL,
PRIMARY KEY (id_bid),
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE,
FOREIGN KEY (id_item) REFERENCES item(id_item) ON DELETE CASCADE
);

-- update the item table to have the highest bid
-- add the column
ALTER TABLE item 
ADD COLUMN id_highest_bid INTEGER;
-- add the foreign key
ALTER TABLE item 
ADD FOREIGN KEY (id_highest_bid) REFERENCES bid(id_bid) ON DELETE CASCADE;

-- Picture
CREATE TABLE picture
(id_picture		INTEGER		NOT NULL,
picture_url		VARCHAR(255)		NOT NULL,
id_item			INTEGER		NOT NULL,
PRIMARY KEY (id_picture),
FOREIGN KEY (id_item) REFERENCES item(id_item) ON DELETE CASCADE
);



-- Bookmark
CREATE TABLE bookmark
(id_user		INTEGER		NOT NULL,
id_item			INTEGER		NOT NULL,
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE,
FOREIGN KEY (id_item) REFERENCES item(id_item) ON DELETE CASCADE
);



-- Payment
CREATE TABLE payment
(id_bid			INTEGER		NOT NULL,
id_user		INTEGER		NOT NULL,
id_item			INTEGER		NOT NULL,
status			INTEGER		NOT NULL,
id_pmethod		INTEGER		NOT NULL,
FOREIGN KEY (id_bid) REFERENCES bid(id_bid) ON DELETE CASCADE,
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE,
FOREIGN KEY (id_item) REFERENCES item(id_item) ON DELETE CASCADE,
FOREIGN KEY (id_pmethod) REFERENCES payment_method(id_pmethod) ON DELETE CASCADE
);

-- Shipping
CREATE TABLE shipping
(id_pmethod			INTEGER		NOT NULL,
id_location			INTEGER		NOT NULL,
status				INTEGER		NOT NULL,
tracking_number		VARCHAR(45)		NOT NULL,
FOREIGN KEY (id_pmethod) REFERENCES payment_method(id_pmethod) ON DELETE CASCADE,
FOREIGN KEY (id_location) REFERENCES location(id_location) ON DELETE CASCADE
);

-- Comment
CREATE TABLE comment
(id_comment		INTEGER		NOT NULL,
id_item			INTEGER		NOT NULL,
id_user		INTEGER		NOT NULL,
comment		VARCHAR(255)		NOT NULL,
PRIMARY KEY (id_comment),
FOREIGN KEY (id_item) REFERENCES item(id_item) ON DELETE CASCADE,
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE
);

-- Chatroom
CREATE TABLE chatroom
(id_chatroom		INTEGER		NOT NULL,
id_bidder		INTEGER		NOT NULL,
id_seller		INTEGER		NOT NULL,
id_item			INTEGER		NOT NULL,
admin_help		INTEGER,
PRIMARY KEY (id_chatroom),
FOREIGN KEY (id_bidder) REFERENCES user(id_user) ON DELETE CASCADE,
FOREIGN KEY (id_seller) REFERENCES user(id_user) ON DELETE CASCADE,
FOREIGN KEY (id_item) REFERENCES item(id_item) ON DELETE CASCADE
);

-- Message
CREATE TABLE message
(message		VARCHAR(255)		NOT NULL,
id_chatroom		INTEGER		NOT NULL,
id_user		INTEGER		NOT NULL,
date_time		DATETIME		NOT NULL,
msg_read		INTEGER		NOT NULL,
FOREIGN KEY (id_chatroom) REFERENCES chatroom(id_chatroom) ON DELETE CASCADE,
FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE CASCADE
);



-- Report
CREATE TABLE report
(id_report			INTEGER		NOT NULL,
id_user_reported		INTEGER		NOT NULL,
id_user_reporting		INTEGER		NOT NULL,
Message			VARCHAR(255),
status				INTEGER,
PRIMARY KEY (id_report),
FOREIGN KEY (id_user_reported) REFERENCES user(id_user) ON DELETE CASCADE,
FOREIGN KEY (id_user_reporting) REFERENCES user(id_user) ON DELETE CASCADE
);