use project;
-- [Sign in]
-- 1. signin user (admin user)
SELECT * from user where email='aliceW@gmail.com' and password=md5('1234');


-- [Category list]
-- 2. count items grouping by category
SELECT A.id_category, B.name, count(*) as count from item as A inner join category B on B.id_category=A.id_category group by A.id_category;

-- 3. get all data from category for drop down menu
SELECT * from category;


-- [Item list]
-- 4. get all data from item.
SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user;

-- 5. Select all the items of Category 'Fridge'
SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user and A.id_category=3;

-- 6. Select all the items which includes text 'refrigerator'
SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user and (A.name like '%monitor%' or A.description like '%monitor%');


-- [Item information]
-- 7. Select all data of an item with pk '2976' and seller's information whom uploaded this item
SELECT * FROM item as A JOIN seller as B ON A.id_user = B.id_user where A.id_item=3;

-- 8. Get all bidding data including payment and shipping details on a specific item
SELECT A.id_bid, A.bid_amount, A.id_user as id_bid_user, A.id_item, A.status as bid_status, B.id_pmethod, B.status as payment_status, C.tracking_number, C.id_location, C.status as shipping_status FROM bid as A INNER JOIN payment as B ON A.id_bid = B.id_bid INNER JOIN shipping as C ON B.id_pmethod = C.id_pmethod where A.id_item=3 order by A.bid_amount DESC;

-- 9. Select highest bid from the current item which has '2' as pk and update the item's id_highest_bid
UPDATE item SET id_highest_bid=(SELECT bid.id_bid FROM bid WHERE bid_amount = (SELECT max(bid_amount) FROM bid WHERE id_item='2')) where id_item=2;


-- [user list]
-- 10. get all the user information and salary of admin with reported number
SELECT A.id_user, A.f_name, A.l_name, A.phone, A.status, A.email, A.id_location, B.salary, (Select count(*) from report as C where C.id_user_reported=A.id_user) as reported_cnt from user As A left outer join admin as B on A.id_user = B.id_user;

-- 11. Delete users where one's reported time has exceeded 5 times.
DELETE from user where id_user in (select id_user_reported FROM report GROUP BY id_user_reported HAVING count(*) > 4);

-- 12. get all the information of the specific user
SELECT A.id_user, A.f_name, A.l_name, A.phone, A.status, A.email, A.id_location, B.salary as admin_salary, B.started_date as admin_started_date, D.seller_nickname, D.id_bank_account as seller_bank_account, E. id_pmethod as bidder_pmethod, (Select count(*) from report as C where C.id_user_reported=A.id_user) as reported_cnt from user As A left outer join admin as B on A.id_user = B.id_user left outer join seller as D on A.id_user=D.id_user left outer join bidder as E on A.id_user=E.id_user where A.id_user=3;

-- 13. Select all the seller B's items' name and other data.
SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user and A.id_user='3';


-- [Chatroom list]
-- 14.select all chatroom
SELECT distinct A.id_chatroom, B.f_name, B.l_name, C.seller_nickname, A.id_item, A.admin_help from chatroom as A, user as B, seller as C where A.id_bidder=B.id_user and A.id_seller=C.id_user;

-- 15.get all message in the specific chatroom
SELECT A.id_chatroom, A.message, B.f_name, B.l_name, A.date_time, A.msg_read from message as A, user as B where id_chatroom=1 and A.id_user=B.id_user order by date_time;

-- 16. Update an message I got from 'unread' to 'read'.
UPDATE message SET msg_read='1' where id_chatroom='1' and id_user='3';









