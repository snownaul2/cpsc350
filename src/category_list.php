<?php
//   include("config.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style>
    #page_num {
      font-size: 14px;
      margin-left: 260px;
      margin-top:30px;
    }
    #page_num ul li {
      float: left;
      margin-left: 10px;
      text-align: center;
    }
    .fo_re {
      font-weight: bold;
      color:red;
    }
    table, tr, th, td{
      border: 1px solid black;
      border-collapse: collapse;
    }
    td, th{
      padding: 5px;
    }
    </style>
    </head>

<body>
<h1>CATEGORY LIST</h1>

<?php

    define('DB_SERVER','35.223.222.79:3306');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', 'password');
    define('DB_DATABASE','project');
    $connect=mysqli_connect(DB_SERVER,DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    $query ="SELECT A.id_category, B.name, count(*) as count from item as A inner join category B on B.id_category=A.id_category group by A.id_category;";
    $result = $connect->query($query);
    $total = mysqli_num_rows($result);
 
?>

<h4>Show all the categories and item counts</h4>

        <h2 align=center>CATEGORY LIST</h2>
        <table align = center>
        <thead align = "center">
        <tr>
              <th>Category ID</th>
              <th>Name</th>
              <th>Item Count</th>
        </tr>
        </thead>
 
        <tbody>
        <?php
                while($rows = mysqli_fetch_assoc($result)){ //DB에 저장된 데이터 수 (열 기준)
                        if($total%2==0){
        ?>                      <tr class = "even">
                        <?php   }
                        else{
        ?>                      <tr>
                        <?php } ?>
                        <td><?php echo $rows['id_category']; ?></td>
                        <td><?php echo $rows['name']?></td>
                        <td><?php echo $rows['count'] ?></span></a></td>
                </tr>
        <?php
                $total--;
                }
        ?>
        </tbody>
        </table>

</body>