<?php
//   include("config.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style>
    #page_num {
      font-size: 14px;
      margin-left: 260px;
      margin-top:30px;
    }
    #page_num ul li {
      float: left;
      margin-left: 10px;
      text-align: center;
    }
    .fo_re {
      font-weight: bold;
      color:red;
    }
    table, tr, th, td{
      border: 1px solid black;
      border-collapse: collapse;
    }
    td, th{
      padding: 5px;
    }
    </style>
    </head>

<body>
<h1>CHATROOM LIST</h1>

<?php

    define('DB_SERVER','35.223.222.79:3306');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', 'password');
    define('DB_DATABASE','project');
    $connect=mysqli_connect(DB_SERVER,DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    $query ="SELECT distinct A.id_chatroom, B.f_name, B.l_name, C.seller_nickname, A.id_item, A.admin_help from chatroom as A, user as B, seller as C where A.id_bidder=B.id_user and A.id_seller=C.id_user;";
    $result = $connect->query($query);
    $total = mysqli_num_rows($result);
 
?>

<h4>Show all the chatrooms</h4>

        <h2 align=center>CHATROOM LIST</h2>
        <table align = center>
        <thead align = "center">
        <tr>
              <th>Chatroom Code</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Nickname</th>
              <th>Item ID</th>
              <th>Admin Help</th>
              <th>-</th>
        </tr>
        </thead>
 
        <tbody>
        <?php
                while($rows = mysqli_fetch_assoc($result)){ //DB에 저장된 데이터 수 (열 기준)
                        if($total%2==0){
        ?>                      <tr class = "even">
                        <?php   }
                        else{
        ?>                      <tr>
                        <?php } ?>
                        <td><?php echo $rows['id_chatroom']; ?></a></td>
                        <td><?php echo $rows['f_name']?></td>
                        <td><?php echo $rows['l_name'] ?></td>
                        <td><?php echo $rows['seller_nickname'] ?></td>
                        <td><?php echo $rows['id_item'] ?></td>
                        <td><?php echo $rows['admin_help'] ?></td>
                        <td>
                          <form action="chatroom_view.php" method="get" target="iframe1" id="hello">
                            <input class='gone' display="none" type="hidden" name="id_chatroom" value="<?php echo $rows['id_chatroom']; ?>" id="hello"/>
                            <button>Show More</button>
                          </form>
                        </td>
                </tr>
        <?php
                $total--;
                }
        ?>
        </tbody>
        </table>

</body>