<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Sign Up</title>
    <style media="screen">
    <style>
      * {margin: 0; padding: 0;}
      #sign_up_box{
        width:400px;
        height:250px;
        border:solid 2px gray;
        position: absolute;
        left: 50%; top: 50%;
        margin-left: -200px;
        margin-top: -100px;
        background-color: orange;
      }
      #sign_up_button{
        position: absolute;
        left: 40%; top: 80%;
      }

    </style>
    <script>
      function passwordCheck(){
          var pw = document.getElementById("pw").value;
          //var pw_ck = document.getElementById("pw_check").value;
          
          // var id_ch = document.getElementById("id_ch").value;
          // var nik_ch = document.getElementById("nik_ch").value;
          if (pw=="")  {
            alert("Enter the password.");
          }
          else{
            document.getElementById("sign").submit();
          }
      }
    </script>
  </head>
  <body>
    <div id="sign_up_box">

    <p>Sign Up</p>

    <form class="" action="sign_up_search.php" method="post" id="sign">
      <table>
        <tr>
          <td>ID</td>
          <td>
            <input type="text" name="id" id="uid">
          </td>
        </tr>
        <tr>
          <td>First Name</td>
          <td>
            <input type="text" name="firstname" id="firstname">
          </td>
        </tr>
        <tr>
          <td>Last Name</td>
          <td>
            <input type="text" name="lastname" id="lastname">
          </td>
        </tr>
        <tr>
          <td>Password</td>
          <td>
            <input type="password" id="pw" name="pw">
          </td>
        </tr>
        <!-- <tr>
          <td>Confirm Password</td>
          <td>
            <input type="password" id="pw_ck" name="pw_check">
          </td>
        </tr> -->
      </table>
      <button id="sign_up_button" type="button" name="button" align="right" onclick="passwordCheck()">Sign Up</button>
    </form>
    </div>
  </body>
</html>
