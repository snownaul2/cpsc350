<?php
  include("config.php");
?>
<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<title>Item-Search</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div id="board_area">

<?php

  /* 검색 변수 */
  $catagory = $_GET['catgo'];
  $search_con = $_GET['search'];
?>
  <h1>Result of '<?php echo $search_con; ?>' in Item List</h1>
  <h4 style="margin-top:30px;"><a href="./item_list.php"> <- go back to Item list</a></h4>
    <table class="list-table">
      <thead>
          <tr>
              <th width="70">item code</th>
              <th width="120">item name</th>
              <th width="500">description</th>
              <th width="100">deadline</th>
              <th width="100">minimum price</th>
            </tr>
        </thead>

        <!-- DB: SEARCH SQL -->
        <?php
          $sql2 = mq("select * from board where $catagory like '%$search_con%' order by idx desc");
          while($board = $sql2->fetch_array()){

          $title=$board["title"];
            if(strlen($title)>30)
              {
                $title=str_replace($board["title"],mb_substr($board["title"],0,30,"utf-8")."...",$board["title"]);
              }
        ?>
        <tbody>
          <tr>
            <td width="70"><?php echo $board['idx']; ?></td>
            <td width="120"><?php echo $board['name']?></td>
            <td width="500"><a href='read.php?board_id=board&idx=<?php echo $board["idx"]; ?>'><span style="background:yellow;"><?php echo $title; ?></span></a></td>
            <td width="100"><?php echo $board['date']?></td>
            <td width="100"><?php echo $board['hit']; ?></td>
          </tr>
        </tbody>

        <?php } ?>
    </table>

    <div id="search_box2">
      <form action="search_result.php" method="get">
      <select name="catgo">
        <option value="title">제목</option>
        <option value="name">글쓴이</option>
        <option value="content">내용</option>
      </select>
      <input type="text" name="search" size="40" required="required"/> <button>검색</button>
    </form>
  </div>
</div>
</body>
</html>
