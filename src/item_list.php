<?php
//   include("config.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style>
    #page_num {
      font-size: 14px;
      margin-left: 260px;
      margin-top:30px;
    }
    #page_num ul li {
      float: left;
      margin-left: 10px;
      text-align: center;
    }
    .fo_re {
      font-weight: bold;
      color:red;
    }
    table, tr, th, td{
      border: 1px solid black;
      border-collapse: collapse;
    }
    td, th{
      padding: 5px;
    }
    .gone{
      display: 'none';
    }
    </style>
    </head>

<body>
<h1>ITEM LIST</h1>

<?php
    $category="";
    $word="";
    if(isset($_REQUEST['category'])){
      $category=$_REQUEST['category'];  
    }
    if(isset($_REQUEST['word'])){
      $word=$_REQUEST['word'];  
    }
    
    define('DB_SERVER','35.223.222.79:3306');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', 'password');
    define('DB_DATABASE','project');
    $connect=mysqli_connect(DB_SERVER,DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    $query="";
    if(!empty($category) && !empty($word)){
      $query="SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user and A.id_category= $category and (A.name like '%$word%' or A.description like '%$word%');";
    }else if(!empty($word)){
      $query="SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user and (A.name like '%$word%' or A.description like '%$word%');";
    }else if(!empty($category)){
      $query="SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user and A.id_category=$category;";
    }else{
      $query ="SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user;";
    }
    $result = $connect->query($query);
    $total = mysqli_num_rows($result);
 
?>

<h4>Show all the bidding items<?php echo $word; ?></h4>
      <div id="search_box">
        <form action="item_list.php" method="get" target="iframe1">
          <select name="category">
            <option value="">select category</option>
          <?php
          
          $query2="SELECT * from category;";
          $result2 = $connect->query($query2);
          $total2 = mysqli_num_rows($result2);
          while($rows = mysqli_fetch_assoc($result2)){ 

            ?>
              <option value="<?php echo $rows['id_category']; ?>"><?php echo $rows['name']; ?></option>
            <?php

            $total2--;
          }          
          ?>

          </select>
          <input type="text" name="word" size="40" placeholder="search item"/> <button>Search</button>
        </form>
      </div>

        <h2 align=center>ITEM LIST</h2>
        <table align = center>
        <thead align = "center">
        <tr>
              <th>Item Code</th>
              <th>Item Name</th>
              <th>Deadline</th>
              <th>Category Code</th>
              <th>Seller Nickname</th>
              <th>Highest Bid Code</th>
              <th>-</th>
        </tr>
        </thead>
 
        <tbody>
        <?php
                while($rows = mysqli_fetch_assoc($result)){ //DB에 저장된 데이터 수 (열 기준)
                        if($total%2==0){
        ?>                      <tr class = "even">
                        <?php   }
                        else{
        ?>                      <tr>
                        <?php } ?>
                        <td><?php echo $rows['id_item']; ?></td>
                        <td><?php echo $rows['name']?></td>
                        <td><?php echo $rows['deadline'] ?></span></a></td>
                        <td><?php echo $rows['id_category']?></td>
                        <td><?php echo $rows['seller_nickname']; ?></td>
                        <td><?php echo $rows['id_highest_bid']; ?></td>
                        <td>
                          <form action="item_view.php" method="get" target="iframe1" id="hello">
                            <input class='gone' display="none" type="hidden" name="id_item" value="<?php echo $rows['id_item']; ?>" id="hello"/>
                            <button>Show More</button>
                          </form>
                        </td>
                </tr>
        <?php
                $total--;
                }
        ?>
        </tbody>
        </table>
</body>