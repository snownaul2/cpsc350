<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style>
    #page_num {
      font-size: 14px;
      margin-left: 260px;
      margin-top:30px;
    }
    #page_num ul li {
      float: left;
      margin-left: 10px;
      /* text-align: center; */
    }
    .fo_re {
      font-weight: bold;
      color:red;
    }
    table, tr, th, td{
      border: 1px solid black;
      border-collapse: collapse;
    }
    td, th{
      padding: 5px;
    }
    </style>
    </head>

<body>

<?php
$id_user="";
if(isset($_REQUEST['id_user'])){
$id_user=$_REQUEST['id_user'];  
}
  include("config.php");
  define('DB_SERVER','35.223.222.79:3306');
  define('DB_USERNAME', 'root');
  define('DB_PASSWORD', 'password');
  define('DB_DATABASE','project');
    $host="";
    $username="root";
    $password="password";
    $db="project";
    $connect=mysqli_connect(DB_SERVER,DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    $query ="SELECT A.id_user, A.f_name, A.l_name, A.phone, A.status, A.email, A.id_location, B.salary as admin_salary, B.started_date as admin_started_date, 
    D.seller_nickname, D.id_bank_account as seller_bank_account, E. id_pmethod as bidder_pmethod, 
    (Select count(*) from report as C where C.id_user_reported=A.id_user) as reported_cnt from user As A left outer join admin as B on A.id_user = B.id_user 
    left outer join seller as D on A.id_user=D.id_user left outer join bidder as E on A.id_user=E.id_user where A.id_user=$id_user;";
    $result = $connect->query($query);
    $rows = mysqli_fetch_assoc($result);
?>
        <!-- MODIFY & DELETE -->
        <div class="view_btn">
                <button class="view_btn1" onclick="location.href='./user_list.php'">back to USER LIST</button>
        </div>

        <h2>User Information</h2>
        <table class="view_table">
        <tr>
                <td class="view_hit">User Code</td>
                <td class="view_hit2"><?php echo $rows['id_user']?></td>
        </tr>
        <tr>
                <td class="view_id">Name</td>
                <td class="view_id2"><?php echo $rows['f_name']?> <?php echo $rows['l_name']?></td>
        </tr>
        <tr>
                <td class="view_id">Phone Number</td>
                <td class="view_id2"><?php echo $rows['phone']?></td>
        </tr>

        <tr>
                <td class="view_id">Status</td>
                <td class="view_id2"><?php echo $rows['status']?></td>
        </tr>

        <tr>
                <td class="view_hit">Email</td>
                <td class="view_hit2"><?php echo $rows['email']?></td>
        </tr>
        <tr>
                <td class="view_hit">Location</td>
                <td class="view_hit2"><?php echo $rows['id_location']?></td>
        </tr>
        <tr>
                <td class="view_hit">Salary</td>
                <td class="view_hit2">$<?php echo $rows['admin_salary']?></td>
        </tr>
        <tr>
                <td class="view_hit">Started Date</td>
                <td class="view_hit2"><?php echo $rows['admin_started_date']?></td>
        </tr>
        <tr>
                <td class="view_hit">Nickname</td>
                <td class="view_hit2"><?php echo $rows['seller_nickname']?></td>
        </tr>
        <tr>
                <td class="view_hit">Bank Account</td>
                <td class="view_hit2"><?php echo $rows['seller_bank_account']?></td>
        </tr>
        <tr>
                <td class="view_hit">Pay Method</td>
                <td class="view_hit2"><?php echo $rows['bidder_pmethod']?></td>
        </tr>
        <tr>
                <td class="view_hit">Reported Count</td>
                <td class="view_hit2"><?php echo $rows['reported_cnt']?></td>
        </tr>
        </table>

        <h2>My Items</h2>
        <table align = center>
        <tr>
              <th>Item Code</th>
              <th>Item Name</th>
              <th>Deadline</th>
              <th>Category</th>
              <th>Nickname</th>
              <th>Highest Bid</th>
        </tr>
        </thead>
 
        <tbody>
        <?php
            $query2="SELECT A.id_item, A.name, A.deadline, A.id_category, B.seller_nickname, A.id_highest_bid from item as A ,seller as B where A.id_user=B.id_user and A.id_user=$id_user;";
            $result2 = $connect->query($query2);
                $total2 = mysqli_num_rows($result2);
                while($rows2 = mysqli_fetch_assoc($result2)){ //DB에 저장된 데이터 수 (열 기준)
                        if($total2%2==0){
        ?>                      <tr class = "even">
                        <?php   }
                        else{
        ?>                      <tr>
                        <?php } ?>
                        <td><?php echo $rows2['id_item']; ?></td>
                        <td>$<?php echo $rows2['name']?></td>
                        <td><?php echo $rows2['deadline'] ?></td>
                        <td><?php echo $rows2['id_category']?></td>
                        <td><?php echo $rows2['seller_nickname']; ?></td>
                        <td><?php echo $rows2['id_highest_bid']; ?></td>
                </tr>
        <?php
                $total2--;
                }
        ?>
        </tbody>
        </table>
</body>
</html>