<?php
//   include("config.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style>
    #page_num {
      font-size: 14px;
      margin-left: 260px;
      margin-top:30px;
    }
    #page_num ul li {
      float: left;
      margin-left: 10px;
      text-align: center;
    }
    .fo_re {
      font-weight: bold;
      color:red;
    }
    table, tr, th, td{
      border: 1px solid black;
      border-collapse: collapse;
    }
    td, th{
      padding: 5px;
    }
    </style>
    </head>

<body>
<h1>USER LIST</h1>

<?php
    
    define('DB_SERVER','35.223.222.79:3306');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', 'password');
    define('DB_DATABASE','project');
    $connect=mysqli_connect(DB_SERVER,DB_USERNAME, DB_PASSWORD, DB_DATABASE);

    $del="";
    if(isset($_REQUEST['del'])){
      $del=$_REQUEST['del'];  
        $query2="DELETE from user where id_user in (select id_user_reported FROM report GROUP BY id_user_reported HAVING count(*) > 4);";
        $result2 = $connect->query($query2);
    }


    $query ="SELECT A.id_user, A.f_name, A.l_name, A.phone, A.status, A.email, A.id_location, B.salary, (Select count(*) from report as C where C.id_user_reported=A.id_user) as reported_cnt from user As A left outer join admin as B on A.id_user = B.id_user;";
    $result = $connect->query($query);
    $total = mysqli_num_rows($result);
 
?>

<h4>Show all the users and report counts</h4>

        <h2 align=center>USER LIST</h2>
        <div id="search_box">
        <form action="user_list.php" method="get" target="iframe1">
          <input type="hidden" name="del" value="yes"/><button>Delete users who were reported more than 4 times</button>
        </form>
      </div>
        <table align = center>
        <thead align = "center">
        <tr>
              <th>id user</th>
              <th>f name</th>
              <th>l name</th>
              <th>phone</th>
              <th>status</th>
              <th>email</th>
              <th>id_location</th>
              <th>salary</th>
              <th>reported_cnt</th>
        </tr>
        </thead>
 
        <tbody>
        <?php
                while($rows = mysqli_fetch_assoc($result)){ //DB에 저장된 데이터 수 (열 기준)
                        if($total%2==0){
        ?>                      <tr class = "even">
                        <?php   }
                        else{
        ?>                      <tr>
                        <?php } ?>
                        <td><?php echo $rows['id_user']; ?></td>
                        <td><?php echo $rows['f_name']?></td>
                        <td><?php echo $rows['l_name'] ?></td>
                        <td><?php echo $rows['phone'] ?></td>
                        <td><?php echo $rows['status'] ?></td>
                        <td><?php echo $rows['email'] ?></td>
                        <td><?php echo $rows['id_location'] ?></td>
                        <td><?php echo $rows['salary'] ?></td>
                        <td><?php echo $rows['reported_cnt'] ?></td>
                        <td>
                          <form action="./user_view.php" method="get" target="iframe1" id="hello">
                            <input class='gone' display="none" type="hidden" name="id_user" value="<?php echo $rows['id_user']; ?>" id="hello"/>
                            <button>Show More</button>
                          </form>
                        </td>
                </tr>
        <?php
                $total--;
                }
        ?>
        </tbody>
        </table>

</body>