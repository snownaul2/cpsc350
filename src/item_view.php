<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style>
    #page_num {
      font-size: 14px;
      margin-left: 260px;
      margin-top:30px;
    }
    #page_num ul li {
      float: left;
      margin-left: 10px;
      text-align: center;
    }
    .fo_re {
      font-weight: bold;
      color:red;
    }
    table, tr, th, td{
      border: 1px solid black;
      border-collapse: collapse;
    }
    td, th{
      padding: 5px;
    }
    </style>
    </head>

<body>

<?php
$id_item="";
if(isset($_REQUEST['id_item'])){
$id_item=$_REQUEST['id_item'];  
}
  include("config.php");
  define('DB_SERVER','35.223.222.79:3306');
  define('DB_USERNAME', 'root');
  define('DB_PASSWORD', 'password');
  define('DB_DATABASE','project');

    $connect=mysqli_connect(DB_SERVER,DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    $query ="SELECT * FROM item as A JOIN seller as B ON A.id_user = B.id_user where A.id_item=$id_item;";
    $result = $connect->query($query);
    $rows = mysqli_fetch_assoc($result);
?>
        <!-- MODIFY & DELETE -->
        <div class="view_btn">
                <button class="view_btn1" onclick="location.href='./item_list.php'">back to ITEM LIST</button>
                <!-- <button class="view_btn1" onclick="location.href='./modify.php?number=<?=$number?>&id=<?=$_SESSION['userid']?>'">수정</button>
                <button class="view_btn1" onclick="location.href='./delete.php?number=<?=$number?>&id=<?=$_SESSION['userid']?>'">삭제</button> -->
        </div>
        <table class="view_table" align=center>
        <tr>
                <td colspan="4" class="view_title"><h2>Item Information</h2></td>
        </tr>
        <tr>
                <td class="view_hit">Item Code</td>
                <td class="view_hit2"><?php echo $rows['id_item']?></td>
        </tr>
        <tr>
                <td class="view_id">Item Name</td>
                <td class="view_id2"><?php echo $rows['name']?></td>
        </tr>
        <tr>
                <td class="view_id">Item Description</td>
                <td class="view_id2"><?php echo $rows['description']?></td>
        </tr>

        <tr>
                <td class="view_id">Item Deadline</td>
                <td class="view_id2"><?php echo $rows['deadline']?></td>
        </tr>

        <tr>
                <td class="view_hit">Item Minimum Price</td>
                <td class="view_hit2">$<?php echo $rows['min_price']?></td>
        </tr>
        <tr>        
                <td class="view_id">Item Category Code</td>
                <td class="view_id2"><?php echo $rows['id_category']?></td>
        </tr>
        <tr>
                <td class="view_hit">Item Seller ID</td>
                <td class="view_hit2"><?php echo $rows['id_user']?></td>
        </tr>
        <tr>
                <td class="view_hit">Highest Bid ID</td>
                <td class="view_hit2"><?php echo $rows['id_highest_bid']?></td>
        </tr>
        <tr>
                <td class="view_hit">Item Seller Nickname</td>
                <td class="view_hit2"><?php echo $rows['seller_nickname']?></td>
        </tr>
        <tr>
                <td class="view_hit">Item Seller Bank Account</td>
                <td class="view_hit2"><?php echo $rows['id_bank_account']?></td>
        </tr>

        </table>
 
 <h2>Biddings</h2>
        <table align = center>
        <thead align = "center">
        <tr>
              <th>id_bid</th>
              <th>bid_amount</th>
              <th>id_bid_user</th>
              <th>id_item</th>
              <th>bid_status</th>
              <th>id_pmethod</th>
              <th>payment_status</th>
              <th>tracking_number</th>
              <th>id_location</th>
              <th>shipping_status</th>
        </tr>
        </thead>
 
        <tbody>
        <?php
            $query2="SELECT A.id_bid, A.bid_amount, A.id_user as id_bid_user, A.id_item, A.status as bid_status, B.id_pmethod, B.status as payment_status, C.tracking_number, C.id_location, C.status as shipping_status FROM bid as A INNER JOIN payment as B ON A.id_bid = B.id_bid INNER JOIN shipping as C ON B.id_pmethod = C.id_pmethod where A.id_item=$id_item order by A.bid_amount DESC;";
            $result2 = $connect->query($query2);
                $total2 = mysqli_num_rows($result2);
                while($rows2 = mysqli_fetch_assoc($result2)){ //DB에 저장된 데이터 수 (열 기준)
                        if($total2%2==0){
        ?>                      <tr class = "even">
                        <?php   }
                        else{
        ?>                      <tr>
                        <?php } ?>
                        <td><?php echo $rows2['id_bid']; ?></td>
                        <td>$<?php echo $rows2['bid_amount']?></td>
                        <td><?php echo $rows2['id_bid_user'] ?></td>
                        <td><?php echo $rows2['id_item']?></td>
                        <td><?php echo $rows2['bid_status']; ?></td>
                        <td><?php echo $rows2['id_pmethod']; ?></td>
                        <td><?php echo $rows2['payment_status']; ?></td>
                        <td><?php echo $rows2['tracking_number']; ?></td>
                        <td><?php echo $rows2['id_location']; ?></td>
                        <td><?php echo $rows2['shipping_status']; ?></td>
                </tr>
        <?php
                $total2--;
                }
        ?>
        </tbody>
        </table>
        
        </body>
        </html>